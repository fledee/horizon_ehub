#########################
### EXECUTION PARAMETERS
###
#solver_settings={'name':'glpk', 'options':['--mipgap','0.05']}
solver_settings={'name':'gurobi', 'options':{'gapRel':0.05}}

root = "/home/fledee/projects/def-revins/fledee/Horizon/"
saveRoot = "/home/fledee/scratch/Horizon/"
dataRoot = root+"Data/"
hubRoot = root+"SysConfig/"
pkgRoot = '/home/fledee/my_packages/'

n_pts = 3 # Number of poits on the Pareto Front (Min 2)

LpStatus = {'Not Solved':0, 'Optimal':1, 'Infeasible':-1, 'Unbounded':-2, 'Undefined':-3}
all_labels = {0:['Cost', 'Investment', 'Operation', 'Carbon', 'Grid', 'PV', 'Boiler', 'HP', 'Inverter', 'time_total', 'time_solver', 'status'],
              1:['Cost', 'Investment', 'Operation', 'Carbon', 'Grid', 'PV', 'Boiler', 'HP', 'Inverter', 'Battery', 'HWT', 'time_total', 'time_solver', 'status']}

scenarios = {0: {'streams': {'from_Grid':{'price':.1, 'co2':.2},
                             'from_Gas':{'price':.02, 'co2':.4},
                             'from_PV':{'export_price':.05}}},
             1: {'streams': {'from_Grid':{'price':.33, 'co2':.6},
                             'from_Gas':{'price':.02, 'co2':.4},
                             'from_PV':{'export_price':.05}}},
             2: {'streams': {'from_Grid':{'price':.1, 'co2':.2},
                             'from_Gas':{'price':.1, 'co2':.4},
                             'from_PV':{'export_price':.05}}},
             3: {'streams': {'from_Grid':{'price':.33, 'co2':.6},
                             'from_Gas':{'price':.1, 'co2':.4},
                             'from_PV':{'export_price':.05}}},
             4: {'streams': {'from_Grid':{'price':.1, 'co2':.2},
                             'from_Gas':{'price':.02, 'co2':.4},
                             'from_PV':{'export_price':.05}}},
             5: {'streams': {'from_Grid':{'price':.33, 'co2':.6},
                             'from_Gas':{'price':.02, 'co2':.4},
                             'from_PV':{'export_price':.05}}},
             6: {'streams': {'from_Grid':{'price':.1, 'co2':.2},
                             'from_Gas':{'price':.1, 'co2':.4},
                             'from_PV':{'export_price':.05}}},
             7: {'streams': {'from_Grid':{'price':.33, 'co2':.6},
                             'from_Gas':{'price':.1, 'co2':.4},
                             'from_PV':{'export_price':.05}}},
             }

bldg_file = {0: {0:'RefBldgResidentialBASE_USA_MN_Duluth.Intl.AP.727450_TMY3.csv',
                 1:"RefBldgResidentialBASE_USA_WA_Seattle-Boeing.Field.727935_TMY3.csv",
                 2:"RefBldgResidentialBASE_USA_MD_Baltimore-Washington.Intl.AP.724060_TMY3.csv",
                 3:"RefBldgResidentialBASE_USA_CA_Los.Angeles.Intl.AP.722950_TMY3.csv",
                 4:"RefBldgResidentialBASE_USA_TX_Houston-William.P.Hobby.AP.722435_TMY3.csv"},
             1: {0:'RefBldgSmallOfficeNew2004_v1.3_7.1_7A_USA_MN_DULUTH.csv',
                 1:"RefBldgSmallOfficeNew2004_v1.3_7.1_4C_USA_WA_SEATTLE.csv",
                 2:"RefBldgSmallOfficeNew2004_v1.3_7.1_4A_USA_MD_BALTIMORE.csv",
                 3:"RefBldgSmallOfficeNew2004_7.1_5.0_3B_USA_CA_LOS_ANGELES.csv",
                 4:"RefBldgSmallOfficeNew2004_v1.3_7.1_2A_USA_TX_HOUSTON.csv"}}

weather_file = {0:"USA_MN_Duluth.Intl.AP.727450_TMY3.epw",
                1:"USA_WA_Seattle-Boeing.Field.727935_TMY3.epw",
                2:"USA_MD_Baltimore-Washington.Intl.AP.724060_TMY3.epw",
                3:'USA_CA_Los.Angeles.Intl.AP.722950_TMY3.epw',
                4:"USA_TX_Houston-William.P.Hobby.AP.722435_TMY3.epw"}

hub_file = {0:"EH_Hub_nS.xlsx", 1:"EH_Hub_wS.xlsx"}



#########################
### LIBRARIES
###
import os
import sys

from time import time

import numpy as np
import pandas as pd
import pickle as pkl

sys.path.insert(0, pkgRoot+'my_packages/pyehub/') 
sys.path.insert(0, pkgRoot)

import excel_to_request_format
from energy_hub.ehub_model import EHubModel
from my_packages.loadEPlus import prepare_building











#############################
### FUNCTIONS FOR EXECUTION
###

#######################
# HANDLE FILES
def create_files_structure(root, args):
    """Create file structure at root location and
    returns path to saving location. """
    building = {0:'Residential', 1:'SmallOffice'}[args['building']]
    hub = {0:'NoStore',1:'WithStore'}[args['hub']]
    zone = {0:'Duluth', 1:'Seattle', 2:'Baltimore', 3:'LosAngeles',4:'Houston'}[args['zone']]
    scenario = f"Scenario{args['scenario']+1}"
    
    #if not os.path.isdir("/".join([root,building])): # Create dir for Buidlings
    #    os.mkdir("/".join([root,building]))
    #if not os.path.isdir("/".join([root,building,hub])): # Create dir for Hubs
    #    os.mkdir("/".join([root,building,hub]))
    #if not os.path.isdir("/".join([root,building,zone])): # Create for Zones
    #    os.mkdir("/".join([root,building,zone]))
    #if not os.path.isdir("/".join([root,building,zone,scenario])): # Create for Zones
    #    os.mkdir("/".join([root,building,zone,scenario]))
        
    savedir = "/".join([root,building,zone,scenario,"/"]).replace("//","/")
    if os.path.isdir(savedir):
        return savedir
    else:
        raise ValueError(f"Folder {savedir} does not exist!")



def save_results(sizing, operation, savedir, kind='full'):
    with open(savedir+f"/sizing_{kind}.pkl","wb") as f:
        pkl.Pickler(f).dump(sizing)
    with open(savedir+f"/operation_{kind}.pkl","wb") as f:
        pkl.Pickler(f).dump(operation)



def load_results(savedir, kind='full'):
    """Load existing results to avoid running the experiments again."""
    with open(savedir+f"/sizing_{kind}.pkl","rb") as f:
        sizing = pkl.Unpickler(f).load()
    with open(savedir+f"/operation_{kind}.pkl","rb") as f:
        operation = pkl.Unpickler(f).load()
    return sizing, operation
        
        
def get_arguments():
    errors = ""
    parameters = {'scenario':None, 'zone':None, 'hub':None, 'building':None}
    for arg in ['--scenario','--zone','--hub','--building']:
        try:
            idx = sys.argv.index(arg)
            try:
                parameters[arg[2:]] = int(sys.argv[idx+1])
            except:
                errors+=f"[{arg}]: Can not turn argument into number.\n"
        except:
            errors+=f"[{arg}]: No such parameter passed.\n"
    
    if errors!="":
        print("\tERRORS FOUND:\n"+errors)
        raise KeyError(errors.replace("\n"," ; "))
    return parameters



#######################
# RESULTS GATHERING
def get_sizes(out, t0=time()):
    sol = {'Cost':out['solution']['total_cost'], 'Investment':out['solution']['investment_cost'],
           'Operation':out['solution']['operating_cost'], 'Carbon': out['solution']['total_carbon'],
           **out['solution']['capacity_tech'], **out['solution']['capacity_storage'],
           'time_solver':out['solver']['time'], 'time_total':time()-t0,
           'status':LpStatus[out['solver']['termination_condition']]}
    return sol


def get_states(out):
    sol = pd.concat( [pd.DataFrame(out['solution']['storage_level']).T.iloc[:-1].rename(columns=lambda c:c+"_SOC"),
                      pd.DataFrame(out['solution']['energy_imported']).T,
                      pd.DataFrame(out['solution']['energy_exported']).T.rename(columns=lambda c:"export_"+c.split("_")[-1]),],
                    axis=1 )
    return sol


#########################
# HANDLE REQUESTS
def set_scenario(request, scenario):
    for s in scenario:
        for i,req in enumerate(request[s]):
            if req['name'] not in list(scenario[s]): continue;
            n = req['name']
            for p in scenario[s][n]:
                request[s][i][p] = scenario[s][n][p]
    return request

def ts_in_request(request, ts):
    for i,req in enumerate(request['time_series']):
        req['data'] = [float(k) for k in ts.iloc[:,i].values]
    return request

def set_request_capacities(request, period_res):
    cap = period_res.loc["Grid":].max(axis=1)
    for req in request['converters']:
        req['capacity'] = float(cap.loc[req['name']])
    for req in request['storages']:
        req['capacity'] = float(cap.loc[req['name']])
    return request

#def set_request_capacities_periods(request, period_res, period_id):
#    cap = period_res.loc["Grid":, period_id]
#    for req in request['converters']:
#        req['capacity'] = float(cap.loc[req['name']])
#    for req in request['storages']:
#        req['capacity'] = float(cap.loc[req['name']])
#    return request

def adapt_request(request, period="d"):
    """Adapts request to shorter horizons."""
    # Informations for the data
    n_periods = {'3M': 4, 'M': 12, 'W': 52, 'd': 365}
    
    # Changes the lifetimes
    for tech in request['converters']: tech['lifetime'] *= n_periods[period]
    for tech in request['storages']: tech['lifetime'] *= n_periods[period]
    return request




#########################
# EPSILON EXECUTIONS
def size_hub(HubClass,request, objective='total_cost', max_carbon=None,):
    """Computes the sizing at one carbon level"""
    # Initialize the energy hub
    t0 = time()
    hub = HubClass(request=request, max_carbon=max_carbon)
    if objective!='total_cost':
        hub.compile()
        hub.objective = objective
    
    # Optimize over the period.
    sol = hub.solve(solver_settings=solver_settings)
    sizes = pd.Series(get_sizes(sol, t0))
    states = get_states(sol)
    return sizes,states


def full_epsilon_constraint(HubClass, excel, scenario, data, n_pts=6, is_verbose=False,):
    """Epsilon Constraint method for the full-case."""
    ## Build the request
    request = ts_in_request(excel_to_request_format.convert(excel), data)
    request = set_scenario(request, scenario)
    
    ## Least cost >> Ref solution at min cost
    if is_verbose: print("Minimum cost...   ", end="\r")
    t0 = time()
    ehub = HubClass(request=request)
    min_cost = ehub.solve(solver_settings=solver_settings)
    
    ## Outcome table
    full_sizes = pd.DataFrame.from_dict({0:get_sizes(min_cost, t0)})
    full_states = {0: get_states(min_cost)}
    
    ## Least CO2 >> Get min CO2 level only (not solution kept as no cost optimal).
    if is_verbose: print("Minimum carbon...", end="\r")
    ehub = HubClass(request=request)
    ehub.compile()
    ehub.objective = 'total_carbon'
    min_carbon=ehub.solve(solver_settings=solver_settings)['solution']['total_carbon']
    
    ## Front: >> All PF fronts AND min carbon based on cost minimizations
    n_slope = max(0,(n_pts-2-1)//2) # nb of points between CO2op and previous regular point
    co2_levels = np.linspace(min_cost['solution']['total_carbon'],min_carbon,n_pts-n_slope)
    co2_levels = np.concatenate( [co2_levels[:-1],
                                  np.linspace(co2_levels[-2],co2_levels[-1],n_slope+2)[1:-1],
                                  co2_levels[-1:]] )[1:] # Insert points for slopy part.
    for n in range(n_pts-1):
        if is_verbose: print(f"Front {n+2}/{n_pts}...         ", end="\r")
        t0 = time()
        ehub = HubClass(request=request, max_carbon=co2_levels[n])
        sol = ehub.solve(solver_settings=solver_settings)
        full_sizes[n+1] = pd.Series(get_sizes(sol, t0))
        full_states[n+1] = get_states(sol)
    if is_verbose: print("                                  ")
        
    return full_sizes.loc[labels,np.arange(n_pts)], full_states # Returns the PF


def period_epsilon_constraint(HubClass, excel, scenario, data, period='d', n_pts=6,
                              is_verbose=False):
    """Sizing within each period"""
    ### Initialize the period-based request (once and for all)
    request = set_scenario(excel_to_request_format.convert(excel), scenario)
    request = adapt_request(request, period)
    
    selection = {'3M': data.index.quarter,
                 'M':  data.index.month,
                 'W': data.index.isocalendar().week,
                 'd': data.index.dayofyear}[period]
    n_periods = {'3M': 4, 'M': 12, 'W': 52, 'd': 365}[period]
    fronts = {n: pd.DataFrame(None, index=labels, columns=np.arange(n_periods))
              for n in range(n_pts)} # 1 front table per carbon level
    states = {n: []
              for n in range(n_pts)} # One Concat of all individual periods per carbon level
    
    ### For each period...
    for p in np.arange(n_periods):
        ### Select the time series chunk
        request = ts_in_request(request, data.loc[selection==p+1])
        
        ### Compute the size front
        if is_verbose: print(f"[{period}] Solve period {p+1}/{n_periods}; front 1/{n_pts}",
                             end="...           \r")
        min_cost, min_cost_states = size_hub(HubClass, request)
        min_co2, _ = size_hub(HubClass, request, objective='total_carbon')
        
        n_slope = max(0,(n_pts-2-1)//2) # nb of points between CO2op and previous regular point
        carbon_front = np.linspace(min_cost.loc['Carbon'], min_co2.loc['Carbon'],n_pts-n_slope)
        carbon_front = np.concatenate( [carbon_front[:-1],
                                        np.linspace(carbon_front[-2],carbon_front[-1],n_slope+2)[1:-1],
                                        carbon_front[-1:]] )[1:] # Insert points for slopy part. (drop first)
        
        fronts[0].loc[:,p] = min_cost # Store min cost sizing
        states[0].append(min_cost_states)
        for n in range(n_pts-1): # Compute sizing for other PF points (incl. min CO2)
            if is_verbose: print(f"[{period}] Solve period {p+1}/{n_periods}; front {n+1}/{n_pts}",
                                 end="...           \r")
            sz,st = size_hub(HubClass, request, max_carbon=carbon_front[n])
            fronts[n+1].loc[:,p] = sz # Store the sizes
            states[n+1].append(st) # Store the states
    if is_verbose: print(f"[{period}] Period sizing done.                                          ")
    return fronts, {n: pd.concat(states[n]).set_index(data.index) for n in states}


def operation_epsilon_constraint(HubClass, excel, scenario, data, fronts, is_verbose=False):
    """Operation for the full time horizon"""
    ### Initialize the operation request
    request = set_scenario(excel_to_request_format.convert(excel), scenario)
    request = ts_in_request(request, data)
    
    results = pd.DataFrame(None, index=fronts[0].index, columns=fronts.keys())
    results_states = {} # To store the SOCs and Energy imports
    for n in fronts.keys(): # For each carbon level
        if is_verbose: print(f"Operation {n+1}/{len(fronts)}... ", end="...         \r")
        ### Set capacities
        request = set_request_capacities(request, period_res=fronts[n])
        ### Optimal operation
        carbon_level = float(fronts[n].loc['Carbon'].sum()) + 1e-2 # Total CO2 + epsilon
        results.loc[:,n], results_states[n] = size_hub(HubClass, request, max_carbon=carbon_level)
    if is_verbose: print(f"Operation done.                             ")
    return results, results_states



def period_operation_epsilon_constraint(HubClass, excel, scenario, data, cap_fronts, period='d', n_pts=6,
                              is_verbose=False):
    """Operation per period"""
    ### Initialize the period-based request (once and for all)
    request = set_scenario(excel_to_request_format.convert(excel), scenario)
    request = adapt_request(request, period)
    
    selection = {'3M': data.index.quarter,
                 'M':  data.index.month,
                 'W': data.index.isocalendar().week,
                 'd': data.index.dayofyear}[period]
    n_periods = {'3M': 4, 'M': 12, 'W': 52, 'd': 365}[period]
    fronts = {n: pd.DataFrame(None, index=labels, columns=np.arange(n_periods))
              for n in range(n_pts)} # 1 front table per carbon level
    states = {n: []
              for n in range(n_pts)} # One Concat of all individual periods per carbon level
    
    ### For each period...
    for p in np.arange(n_periods):
        ### Select the time series chunk
        request = ts_in_request(request, data.loc[selection==p+1])
        # Set capacities in request for min cost case.
        request = set_request_capacities(request, period_res=cap_fronts[0]) # Set max capacity
        
        ### Compute the size front
        if is_verbose: print(f"[{period}] Solve period {p+1}/{n_periods}; front 1/{n_pts}",
                             end="...           \r")
        min_cost, min_cost_states = size_hub(HubClass, request)
        
        fronts[0].loc[:,p] = min_cost # Store min cost sizing
        states[0].append(min_cost_states)
        for n in range(n_pts-1): # Compute sizing for other PF points (incl. min CO2)
            if is_verbose: print(f"[{period}] Solve period {p+1}/{n_periods}; front {n+1}/{n_pts}",
                                 end="...           \r")
            # Set capacities in request
            request = set_request_capacities(request, period_res=cap_fronts[n+1]) # Set max capacity
            ### Optimal operation
            carbon_level = float(cap_fronts[n+1].loc['Carbon',p].sum()) + 1e-2 # Total CO2 + epsilon
            sz,st = size_hub(HubClass, request, max_carbon=carbon_level)
            fronts[n+1].loc[:,p] = sz # Store the sizes
            states[n+1].append(st) # Store the states
    if is_verbose: print(f"[{period}] Period operation done.                                          ")
    return fronts, {n: pd.concat(states[n]).set_index(data.index) for n in states}








##############################
##############################
### MAIN EXECUTION
###
if __name__=='__main__':
    ### GATHER PARAMETERS AND LOAD BUILDING DATA
    args = get_arguments()
    savedir = create_files_structure(saveRoot,args)
    labels = all_labels[args['hub']]
    print(f"Started {args}          ")

    ### Load the building data
    bldg_path = dataRoot + bldg_file[args['building']][args['zone']]
    wthr_path = dataRoot + weather_file[args['zone']]
    bat = prepare_building(path_epw=wthr_path, path_building=bldg_path, withHeat=True,
                           withCold=False, include_wind=False, is_verbose=False, year=2011)
    ### Get scenario parameters
    scenario = scenarios[args['scenario']]
    ### Get the ehub address
    excel = hubRoot + hub_file[args['hub']]


    ### EXECUTE THE EXPERIMENT
    ### Execute normal
    full_sizing, full_status = full_epsilon_constraint(EHubModel, excel, scenario,
                                                       bat, n_pts=n_pts, is_verbose=True)
    save_results(full_sizing, full_status, savedir, kind='full')

    ### Execute for each period
    for period in ['d','W','M','3M']:
        ### Sizing per period
        sizes, sizes_status = period_epsilon_constraint(EHubModel, excel, scenario,
                                                        data=bat, period=period, n_pts=n_pts,
                                                        is_verbose=True)
        save_results(sizes, sizes_status, savedir, kind="sizes"+period)
        ### Load existing results (re-perform some computations)
        #sizes, sizes_status = load_results(savedir, kind="sizes"+period)
        
        ### Operation per period
        perOpe, perOpe_status = period_operation_epsilon_constraint(EHubModel, excel, scenario,
                                                                    data=bat, cap_fronts=sizes, period=period,
                                                                    n_pts=n_pts, is_verbose=True)
        save_results(perOpe, perOpe_status, savedir, kind="perOpe"+period)

        ### Operation total
        ope, ope_status = operation_epsilon_constraint(EHubModel, excel, scenario,
                                                       data=bat,fronts=sizes, is_verbose=True)
        save_results(ope, ope_status, savedir, kind="ope"+period)
