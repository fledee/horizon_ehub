# Title of the paper/experiment
This repository hosts the code related to a publication for the eSim22 conference.

## 1. Description
This study performs and analyses energy system sizing and operation for different time series horizons (day to 3 months). The energy systems are modeled as energy hubs. The experiment is subdivided into two steps: a sizing step and an operation step (Figure 1). In each step, the time line is subdivided in periods of various lengths relative to the different time horizons. During the sizing step, optimal device capacities and operations are computed over each period. During the operation step, the highest capacities obtained over all periods (of equal length horizon) are then used and the resulting energy system is operated, both for one year of hourly data and independently in each shorter period length, estimating the cost and carbon emissions again.

![Methodology](Analysis/pictures/Horizon_Methodology.png)

Figure 1: Workflow

The energy system to be sized is shown in Figure 2, and includes photovoltaic (PV) pannels, a heat-pump (HP), Boiler, a hot water tank (HWT) and a battery.

![system](Analysis/pictures/Horizon_System.png)

Figure 2: Used energy system layout

The study is conducted for 5 locations in different climate zones (Duluth MN, Seattle WA, Baltimore MD, Los Angeles CA, Houston TX), using data from [Hourly Load Profiles of Commercial and Residential Buildings
dataset](https://data.openei.org/submissions/153). In each location, 2 building types are considered (residential and small-office) across 4 scenarios (Table 1). Other modeling parameters were based on energy hub studies and manufacturers data.


## 2. Table for the modeling parameters
Table 1: Technical and financial modeling parameters used.
|                          | PV   |   Boiler |     HP |   Battery |    HWT |
|:-------------------------|:-----|---------:|-------:|----------:|-------:|
| Fixed costs (\$)         | 0    |  1492    | 2907   |      0    | 400    |
| Linear costs (\$/kW(h))  | 850  |    47    |  198   |    700    | 100    |
| Lifetime (y)             | 20   |    25    |   20   |     15    |  20    |
| efficiency               | (\*) |     0.95 |    3.8 |    nan    | nan    |
| (Dis)charging efficiency | nan  |   nan    |  nan   |      0.96 |   0.98 |
| Decay                    | nan  |   nan    |  nan   |      0.01 |   0.05 |
| Ramping $\epsilon$       | nan  |   nan    |  nan   |      0.3  |   0.3  |


Note that the PV potential is precomputed using Gsee package. The efficiency is function of the outside temperature and incidence angle of the sun on the panel. We chose a south oriented panel with a 30° angle.


## 3. Table for the scenarios
Table 2: Scenarios for the study.
|    |   ('Price (\\$/kWh)', 'Electricity') |   ('Price (\\$/kWh)', 'Gas') |   ('Price (\\$/kWh)', 'Feed-in') |   ('Carbon ($gCO_2 / kWh$)', 'Electricity') |   ('Carbon ($gCO_2 / kWh$)', 'Gas') |
|---:|-------------------------------------:|-----------------------------:|---------------------------------:|--------------------------------------------:|------------------------------------:|
|  5 |                                 0.1  |                         0.02 |                             0.05 |                                         200 |                                 400 |
|  6 |                                 0.33 |                         0.02 |                             0.05 |                                         600 |                                 400 |
|  7 |                                 0.1  |                         0.1  |                             0.05 |                                         200 |                                 400 |
|  8 |                                 0.33 |                         0.1  |                             0.05 |                                         600 |                                 400 |


## 4. Structure of the repository

This repository has the following structure:

***experiment_2.py***: executable file performing the entire experiment. It was not adapted to being directly workable from this repository and refers to external packages. External dependencies are the [pyehub](https://pypi.org/project/pyehub/) library (subpackage of the [besos](https://pypi.org/project/besos/) python package), [gsee](https://pypi.org/project/gsee/) package and [loadEPlus](no_link) package, all three gathered under the local `my_packages` package. The latter only applies Gsee on weather data, select relevant building data and merges time series from both source files. Finally the experiment is built to use Gurobi solver, which is a proprietary software. Though the `glpk` package can be used instead adn provide a free solver.

**Data/**: Contains all weather files and building data used. Building data come from the open-source [Hourly Load Profiles of Commercial and Residential Buildings dataset](https://data.openei.org/submissions/153). Weather data are the same as used in the building dataset and are available for free on the [EnergyPlus website](https://energyplus.net/weather).

**Sysconfig/**: Contains the files to initialize the Energy Hubs (configuration exposed in Figure 2 and parameters introduced in Tables 1 and 2). The file used in the experiment is named `EH_Hub_wS.xlsx`. Note that we also have a configuration with no storage (`EH_Hub_nS.xlsx`), related to scenarios 1 to 4. The results are not exposed in the study.

**Results/**: Contains all the results in a structured manner under a collection of pickle files. Note that we also have scenarios 1 to 4, but these were disregarded in the study.

**Analysis/**: Contains all files for analysis as python notebook files. The analysis reported in the study is under `Analysis/yesStorage/`.
